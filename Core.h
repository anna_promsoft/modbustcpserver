#ifndef CORE_H
#define CORE_H

#include <QObject>
#include "qnamespace.h"

#include "modbus.h"

class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(const QString& mbHost, int mbPort,
                  int bits,int inputBits,int registers,int inputRegisters,
                  QObject *parent = 0,const char *name = 0);
    
    void close_sigint(int dummy);
    bool isListening()
    {
        return (server_socket == -1)? false : true;
    }

signals:
    
public slots:

private slots:
    void processModBus();

private:
    // modbus server communication
    modbus_t *ctx;
    modbus_mapping_t *mb_mapping;
    int server_socket;
    uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
    int master_socket;
    int rc;
    fd_set refset;
    fd_set rdset;
    int fdmax;//Maximum file descriptor number
//Ann for write debug    int16_t imitValue;
};

#endif // CORE_H
