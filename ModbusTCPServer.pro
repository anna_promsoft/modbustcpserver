#-------------------------------------------------
#
# Project created by QtCreator 2016-11-12T19:50:49
#
#-------------------------------------------------

QT       += core  network

TARGET = mbtcpd
CONFIG   += console
CONFIG   += qt

TEMPLATE = app

SOURCES += main.cpp \
    Core.cpp \
    modbus-tcp.c \
    modbus-rtu.c \
    modbus-data.c \
    modbus.c \
    ApplicationSettings.cpp


HEADERS += \
    Core.h \
    modbus-version.h.in \
    modbus-version.h \
    modbus-tcp-private.h \
    modbus-tcp.h \
    modbus-rtu-private.h \
    modbus-rtu.h \
    modbus-private.h \
    modbus.h \
    ApplicationSettings.h \
    ../lib/plog/Log.h \
    CoreService.h



unix {
  UI_DIR = .UI
  MOC_DIR = .MOC
  OBJECTS_DIR = .OBJ
  LIBS += -s
}

win32 {
  UI_DIR = UI
  MOC_DIR = MOC
  OBJECTS_DIR = OBJ
#For MinGW
  LIBS += -lws2_32
}

INCLUDEPATH +=  ./ ../lib  ../lib/plog
LIBS += -L../lib/
# you need to adjust this part for your compiler toolset
#For Visual Studio
#LIBS += "C:/Program Files/Microsoft SDKs/Windows/v7.0A/Lib/WS2_32.lib"
QTSERVICE_DIR=../lib/qt-solutions/qtservice/src
include($$QTSERVICE_DIR/qtservice.pri)

