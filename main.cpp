/*#include <QCoreApplication>
#include <QDesktopServices>
#include <QProcessEnvironment>
#include <QStringList>
#include <QtDebug>
#include "ApplicationSettings.h"
#include "Core.h"
#include "Log.h"
*/
#include <QDir>

#include <QSettings>
#include "CoreService.h"


int main(int argc, char **argv)
{
    QCoreApplication::setApplicationName("MbTCP");
#if !defined(Q_OS_WIN)
    // QtService stores service settings in SystemScope, which normally require root privileges.
    // To allow testing this example as non-root, we change the directory of the SystemScope settings file.
    QSettings::setPath(QSettings::NativeFormat, QSettings::SystemScope, QDir::tempPath());
    qWarning("(Example uses dummy settings file: %s/QtSoftware.conf)", QDir::tempPath().toLatin1().constData());
#endif
//    fprintf(stderr,"argv = %s\n", *argv);
    fprintf(stderr,"main\n");
    qDebug() << "In main \n";

    CoreService service(argc, argv);
    return service.exec();

}
