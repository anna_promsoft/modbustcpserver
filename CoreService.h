#ifndef CORESERVICE_H
#define CORESERVICE_H

#include <QtService>
#include <QCoreApplication>
#include <QDesktopServices>
#include <QProcessEnvironment>
#include <QStringList>
#include <QDir>
#include <QDebug>
#include "Core.h"
#include "ApplicationSettings.h"
#include "Log.h"



class CoreService : public QtService<QCoreApplication>
{
public:
    CoreService(int argc, char **argv)
        : QtService<QCoreApplication>(argc, argv, "MbTCP")
        {
            setServiceDescription("A modbus server implemented with Qt");
            setServiceFlags(QtServiceBase::CanBeSuspended);
        }
    ~CoreService() {}

protected:
    void start()
    {
        QCoreApplication *app = application();
/*
#if QT_VERSION < 0x040100
        quint16 param = (app->argc() > 1) ?
                QString::fromLocal8Bit(app->argv()[1]).toUShort() : 8080;
#else
        const QStringList arguments = QCoreApplication::arguments();
        quint16 param = (arguments.size() > 1) ?
                arguments.at(1).toUShort() : 8080;
#endif
*/
        int ini_error = initAppSettings(app);
//initialize logging
        QString lf = appSettings.logFilePath();
        fprintf(stderr,"start: ini_error(Hex) = %x appSettings.logFilePath() = %s\n",ini_error,lf.toLatin1().data());
        initLogging(lf);
qDebug() << QString("MbTCP service version %1 starting").arg(APP_VERSION);
        LOG_INFO << (QString("Starting MbTCP service version ") + APP_VERSION + ".\n").toLatin1().data();
//Have look at ini_error
        switch ( ini_error ) {
        case 0:
            LOG_INFO << QString("Settings taken from %1\n").arg(appSettings.iniFilePath()).toLatin1().data();
            fprintf(stderr,"Settings taken from %s\n",appSettings.iniFilePath().toLatin1().data());
            break;
        case 1:
            LOG_INFO << QString("Failed to find project settings at %1.\n").arg(appSettings.iniFilePath()).toLatin1().data();
            fprintf(stderr,"Failed to find project settings at %s.\n",appSettings.iniFilePath().toLatin1().data());
          break;
        case 2:
            LOG_INFO << QString("Can't open roject settings file at %1. Default settings will be used.\n").arg(appSettings.iniFilePath()).toLatin1().data();
            fprintf(stderr,"Can't open roject settings file at %s. Default settings will be used",appSettings.iniFilePath().toLatin1().data());
          break;
        default:
          break;
        }

        coreDaemon = new Core(appSettings.host(),
                              appSettings.port(),
                              appSettings.bits(),
                              appSettings.inputBits(),
                              appSettings.registers(),
                              appSettings.inputRegisters(),
                              0,"mbtcpd");
        if (!coreDaemon->isListening()) {
            LOG_INFO << QString("Failed to bind to port %1.\n").arg(appSettings.port()).toLatin1().data();
              app->quit();
        }
        else
        {
            LOG_INFO << (QString("MbTCP service version ") + APP_VERSION + " started.\n").toLatin1().data();
            fprintf(stderr,"MbTCP service version %s started \n", APP_VERSION);
        }
    }//start
    void stop()
    {
        QtService<QCoreApplication>::stop();
        LOG_INFO << (QString("MbTCP service version ") + APP_VERSION + " stopped.\n").toLatin1().data();
    }
    void pause() { QtService<QCoreApplication>::pause(); }
    void resume() { QtService<QCoreApplication>::resume(); }

private:
    void initLogging(QString& logFilePath);
    int initAppSettings(QCoreApplication *app);
    ApplicationSettings appSettings;
    Core* coreDaemon;
};//CoreService




void CoreService::initLogging(QString& logFilePath)
{
    plog::init(plog::info, logFilePath.toLatin1(),5000,3); // Initialize the logger.
//LOG_INFO << QString("CoreService::initLogging after plog::init").toLatin1().data();
    fprintf(stderr,"CoreService::initLogging pathToLogFile = %s\n ", logFilePath.toLatin1().data());
}//initLogging

int CoreService::initAppSettings(QCoreApplication* app)
{
    int error = 0;
//make pure name of exe file;
    QString nm = QCoreApplication::applicationFilePath();
fprintf(stderr,"nm = %s\n",nm.toLatin1().data());
    int k = nm.lastIndexOf(".");
    if(k>0)
        nm = nm.left(k);
fprintf(stderr,"nm = %s\n",nm.toLatin1().data() );
    k = nm.lastIndexOf("/");
    if(k>0)
        nm = nm.right(nm.size()-k-1);//app executable without extention and dir path
fprintf(stderr,"k =%d nm = %s\n",k,nm.toLatin1().data() );

qDebug() << "CoreService::initAppSettings(QCoreApplication* app)";
    QStringList args = app->arguments();
    //ApplicationSettings appSettings;
    if (args.length() == 7)//getting settings from command line\\Do not use it yet, it's not done compkletely yet, use ini or default settings
    {
qDebug() << "CoreService::initAppSettings(QCoreApplication* app): args.length() = 7";
//This has to be done later on, it's not full done yet
        appSettings.setHost(args.at(1));
        appSettings.setPort(args.at(2).toInt());
        appSettings.setBits(args.at(3).toInt());
        appSettings.setInputBits(args.at(4).toInt());
        appSettings.setRegisters(args.at(5).toInt());
        appSettings.setInputRegisters(args.at(6).toInt());
    }
    else if(args.length() == 3)//getting settings from command line: just host & port
    {
        appSettings.setHost(args.at(1));
        appSettings.setPort(args.at(2).toInt());
    }
    else    //initialize settings from .ini file
    {
qDebug() << "CoreService::initAppSettings(QCoreApplication* app): args.length() != 7, initialize settings from .ini file";

//get .ini & def .log file paths
        QString pathToIniFile = QCoreApplication::applicationDirPath();
        QDir pathIni(pathToIniFile);
#ifdef Q_WS_WIN
        pathIni.cdUp();//1 step up
        pathToIniFile = pathIni.filePath(nm+".ini");
#endif
        int error;
//initialize settings from .ini file
        appSettings = ApplicationSettings::load(pathToIniFile,nm,&error);
fprintf(stderr,"main after ApplicationSettings::load: error = %d\n", error);
    }
//check Log file path setting

    QString logFilePath = appSettings.logFilePath();
fprintf(stderr,"after load: logFilePath = %s\n",appSettings.logFilePath().toLatin1().data() );
    if(logFilePath.isEmpty() || (!logFilePath.isEmpty() && !QDir(logFilePath).exists())) //use def log path
        logFilePath = QProcessEnvironment::systemEnvironment().value("PROGRAMDATA");//APPDATA");//if not - def value is returned
fprintf(stderr,"after anal logFilePath = %s\n",logFilePath.toLatin1().data() );

    QDir pathLog(logFilePath);
    if(!pathLog.exists())
    {
        logFilePath.clear();
        error += 0x10;
    }
    else
    {
        logFilePath = pathLog.path() + "/" + QCoreApplication::applicationName() + "/" + nm + ".log";
        if(!QDir(pathLog.path() + "/" + QCoreApplication::applicationName()).exists())
            if(!pathLog.mkdir(QCoreApplication::applicationName()))
            {
                logFilePath.clear();
                error += 0x20;
            }
     }
fprintf(stderr,"after anal & create sub: logFilePath = %s\n",logFilePath.toLatin1().data() );
    appSettings.setLogFilePath(logFilePath);

//    fprintf(stderr,"main: bits = %d, inputBits = %d, registers = %d, inputRegisters = %d\n", bits,inputBits,registers,inputRegisters);
//    QString msg = QString("main: bits = %1, inputBits = %2, registers = %3, inputRegisters = %4\n")
//        .arg(bits).arg(inputBits).arg(registers).arg(inputRegisters);
//    LOG_INFO << QString("main: bits = %1, inputBits = %2, registers = %3, inputRegisters = %4\n")
//                .arg(bits).arg(inputBits).arg(registers).arg(inputRegisters).toLatin1().data();
    return error;
}//initAppSettings



#endif // CORESERVICE_H
