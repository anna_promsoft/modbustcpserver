#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H
#include <QString>
#include <QStringList>
#include <QCoreApplication>
#include <modbus.h>

#define HOST "127.0.0.1"
#define PORT 502
#define BITS                2*MODBUS_MAX_READ_REGISTERS   //(Ann: i got max size defined in modbus.h)size of input registers memory 0X to allocate   (bytes)
#define INPUT_BITS          2*MODBUS_MAX_READ_REGISTERS   //size of input registers memory 1X to allocate   (bytes)
#define REGISTERS           MODBUS_MAX_READ_REGISTERS     //size of holding registers memory 4X to allocate  (registers of 2 bytes)
#define INPUT_REGISTERS     MODBUS_MAX_READ_REGISTERS     //size of input registers memory 3X to allocate    (registers of 2 bytes)
#define DO_LOGGING          true                          //is not implemented yet
#define APP_VERSION         "1.0"                         //the version of this application

class ApplicationSettings
{
public:
    ApplicationSettings();
    static ApplicationSettings load(const QString& iniPath,const QString& nm,int* error);
    QString     host() const;
    int         port() const;
    int         bits() const;
    int         inputBits() const;
    int         registers() const;
    int         inputRegisters() const;
    bool        doLogging() const;
    QString     logFilePath() const;
    QString     iniFilePath() const;
    void        setHost(const QString& host);

    void        setPort(const int port);
    void        setBits(const int bits);
    void        setInputBits(const int inputBits);
    void        setRegisters(const int registers);
    void        setInputRegisters(const int inputRegisters);
    void        setLogging(const bool log);
    void        setLogFilePath(const QString& path);
    void        setIniFilePath(const QString& path);

private:
    QString     m_host;
    QString     log_file_path;
    QString     ini_file_path;
    int         m_port;
    int         m_bits;
    int         m_inputBits;
    int         m_registers;
    int         m_inputRegisters;
    bool        do_logging;
};

#endif // APPLICATIONSETTINGS_H
