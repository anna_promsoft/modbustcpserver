#include <QtCore>
#include "Core.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include "modbus.h"
#include <ws2tcpip.h>
#include <Log.h>

#define NB_CONNECTION 5 //The maximum number of pending connection requests queued for any listening socket
//by default the defined in socket.h SOMAXCONN value will be taken

Core::Core(const QString& mbHost, int mbPort,
           int bits, int inputBits, int registers, int inputRegisters,
           QObject *parent,const char *name): QObject(parent)
                                , ctx(0), server_socket(-1)
{
    //Ann debug 07.10.20ctx = modbus_new_tcp(mbHost.toAscii().data(), mbPort);
ctx = modbus_new_tcp(0, mbPort);

    fprintf(stderr, "Core::Core:  bits = %d, inputBits = %d, registers = %d, inputRegisters = %d\n", bits, inputBits, registers, inputRegisters);

    mb_mapping = modbus_mapping_new(bits, inputBits, registers, inputRegisters);
    if (mb_mapping == NULL)
    {
        LOG_INFO << QString("Failed to allocate the mapping: %1.\n").arg(modbus_strerror(errno)).toLatin1().data();
        fprintf(stderr, "Failed to allocate the mapping: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return;
    }
//modbus_set_debug(ctx,1);//Ann for debug
    server_socket = modbus_tcp_listen(ctx, NB_CONNECTION);
    if (server_socket == -1)
    {
        LOG_INFO << QString("Unable to listen TCP connection.\n").toLatin1().data();
        fprintf(stderr, "Unable to listen TCP connection\n");
        modbus_free(ctx);
        return;
    }

    //signal(SIGINT, close_sigint);

    /* Clear the reference set of socket */
    FD_ZERO(&refset);
    /* Add the server socket */
    FD_SET(server_socket, &refset);

    /* Keep track of the max file descriptor */
    fdmax = server_socket;
    QTimer *timer = new QTimer(this);// modbus update timer
    connect(timer, SIGNAL(timeout()), this, SLOT(processModBus()));
    timer->start();
}

void Core::close_sigint(int dummy)
{
    if (server_socket != -1) {
        closesocket(server_socket);
    }
    modbus_free(ctx);
    modbus_mapping_free(mb_mapping);

    exit(dummy);
}

void Core::processModBus()
{
    timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 50000;

    rdset = refset;
    if (select(fdmax+1, &rdset, NULL, NULL, &timeout) == -1) {
        perror("Server select() failure.");
        close_sigint(1);
    }

    /* Run through the existing connections looking for data to be read */
    for (master_socket = 0; master_socket <= fdmax; master_socket++)
    {
        if (!FD_ISSET(master_socket, &rdset)) {
            continue;
        }
        if (master_socket == server_socket) {
            /* A client is asking a new connection */
            socklen_t addrlen;
            struct sockaddr_in clientaddr;
            int newfd;

            /* Handle new connections */
            addrlen = sizeof(clientaddr);
            memset(&clientaddr, 0, sizeof(clientaddr));
            newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
            if (newfd == -1) {
                perror("Server accept() error");
            } else {
                FD_SET(newfd, &refset);

                if (newfd > fdmax) {
                    /* Keep track of the maximum */
                    fdmax = newfd;
                }
                printf("New connection from %s:%d on socket %d\n",
                       inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port, newfd);
            }
        } else {
            modbus_set_socket(ctx, master_socket);
            rc = modbus_receive(ctx, query);

/*for (int qq=0; qq<20; qq++) qDebug() << query[qq]; //OK
qDebug() << "rc =  " << rc << "\n\n"; //OK
*/
            if (rc > 0) {
                modbus_reply(ctx, query, rc, mb_mapping);
            } else if (rc == -1) {
                /* This example server in ended on connection closing or any errors. */
//                printf("Connection closed on socket %d\n", master_socket);
                closesocket(master_socket);

                /* Remove from reference set */
                FD_CLR(master_socket, &refset);

                if (master_socket == fdmax) {
                    fdmax--;
                }
            }
        }
    }
}

