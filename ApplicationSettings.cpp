#include "ApplicationSettings.h"
#include <QDesktopServices>
#include <QDir>
#include <QSettings>
//#include "Log.h"

ApplicationSettings::ApplicationSettings()
{
}

ApplicationSettings ApplicationSettings::load(const QString &iniFilePath,const QString& nm, int* error)
{
    ApplicationSettings s;
    *error = 0;
//Set default settings first
    s.setHost(QVariant(HOST).toString());
    s.setPort(QVariant(PORT).toInt());
    s.setBits(QVariant(BITS).toInt());
    s.setInputBits(QVariant(INPUT_BITS).toInt());
    s.setRegisters(QVariant(REGISTERS).toInt());
    s.setInputRegisters(QVariant(INPUT_REGISTERS).toInt());
    s.setLogging(QVariant(DO_LOGGING).toInt());
    s.setLogFilePath(QString());
    s.setIniFilePath(QVariant(iniFilePath).toString());

//Check ini file
    QFile file(iniFilePath);
fprintf(stderr,"ApplicationSettings::pathToIniFile = %s\n", iniFilePath.toLatin1().data());
    if (!file.exists())
    {
        *error = 1;
        return s;
    }

//Found settings. Try to read settings.
    if (file.open(QIODevice::ReadOnly))
    {
fprintf(stderr,"ApplicationSettings::Opened project settings file at %s. \n",iniFilePath.toLatin1().data());
        QSettings settings(iniFilePath, QSettings::IniFormat);
        settings.beginGroup("MAIN_SETTINGS");
        s.setHost(settings.value("host",QVariant(HOST)).toString());
        s.setPort(settings.value("port",QVariant(PORT)).toInt());
        s.setBits(settings.value("BitsMemorySize",QVariant(BITS)).toInt());
        s.setInputBits(settings.value("InputBitsMemorySize",QVariant(INPUT_BITS)).toInt());
        s.setRegisters(settings.value("RegistersMemorySize",QVariant(REGISTERS)).toInt());
        s.setInputRegisters(settings.value("InputRegistersMemorySize",QVariant(INPUT_REGISTERS)).toInt());
        settings.endGroup();
        settings.beginGroup("LOGGING");
        s.setLogging(settings.value("DoLogging",QVariant(DO_LOGGING)).toBool());
        s.setLogFilePath(settings.value("LogPath",QVariant("")).toString());
        settings.endGroup();
    }
    else
    {
//    fprintf(stderr,"Can't open roject settings file at %s. Default settings will be used",iniFilePath.toLatin1().data());
        *error = 2;
    }
fprintf(stderr,"ApplicationSettings::load: bits = %d, inputBits = %d, registers = %d, inputRegisters = %d\n", s.bits(),s.inputBits(),s.registers(),s.inputRegisters());
    return s;
}


QString ApplicationSettings::host() const
{
    return m_host;
}

void ApplicationSettings::setHost(const QString &host)
{
    m_host = host;
}

int ApplicationSettings::port() const
{
    return m_port;
}

void ApplicationSettings::setPort(int port)
{
    m_port = port;
}

int ApplicationSettings::bits() const
{
    return m_bits;
}

void ApplicationSettings::setBits(const int bits)
{
    m_bits = bits;
}

int ApplicationSettings::inputBits() const
{
    return m_inputBits;
}

void ApplicationSettings::setInputBits(const int inputBits)
{
    m_inputBits = inputBits;
}


int ApplicationSettings::registers() const
{
    return m_registers;
}

void ApplicationSettings::setRegisters(const int registers)
{
    m_registers = registers;
}

int ApplicationSettings::inputRegisters() const
{
    return m_inputRegisters;
}

void ApplicationSettings::setInputRegisters(int inputRegisters)
{
    m_inputRegisters = inputRegisters;
}

bool ApplicationSettings::doLogging() const
{
    return do_logging;
}

QString ApplicationSettings::logFilePath() const
{
    return log_file_path;
}

QString ApplicationSettings::iniFilePath() const
{
    return ini_file_path;
}

void ApplicationSettings::setLogging(const bool log)
{
    do_logging = log;
}

void ApplicationSettings::setLogFilePath(const QString& path)
{
    log_file_path = path;
}

void ApplicationSettings::setIniFilePath(const QString& path)
{
    ini_file_path = path;
}

